// Checking
// 1.

function computeMultiperse(number: number) {
  let multiperse = 0;
  let current = number;

  while (current > 9) {
    multiperse++;
    let currentComponents = current.toString().split("").map(Number);
    current = currentComponents.reduce((prev, curr) => prev * curr);
  }

  return multiperse;
}

console.log(`487 has a multiperse of ${computeMultiperse(487)}`);

// 2.

function findLowestMP7() {
  let candidate = 10;

  while (computeMultiperse(candidate) < 7) candidate++;

  return candidate;
}

console.log(`The lowest int with MP of 7 is ${findLowestMP7()}`);

// 3.

function computeMPspread(limit: number) {
  const multiperses: Record<number, number> = [];

  for (let candidate = 0; candidate < limit; candidate++) {
    const result = computeMultiperse(candidate);
    multiperses[result] = multiperses[result] ? multiperses[result] + 1 : 1;
  }

  return multiperses;
}

console.log(`The repartition of multiperses for ints from 0 to 1 million is`);
console.table(computeMPspread(1e6));

// Searching
// 1.

function computeMPspreadOptimized(limit: number) {
  const multiperses: Record<number, number> = [];

  for (let candidate = 10; candidate < limit; candidate++) {
    if (candidate.toString().includes("0") || candidate.toString().includes("5")) continue;

    const result = computeMultiperse(candidate);
    multiperses[result] = multiperses[result] ? multiperses[result] + 1 : 1;
  }

  return multiperses;
}

console.log(`The optimized repartition of multiperses for ints from 0 to 100 million is (should take <20s)`);
console.table(computeMPspreadOptimized(1e8));

// Out of curiosity

console.log("Comparing optimized and raw performances on 10M ints");
console.time("Raw");
computeMPspread(1e7);
console.timeEnd("Raw");

console.time("Opti");
computeMPspreadOptimized(1e7);
console.timeEnd("Opti");

// 2.
// the candidate will probably just exclude 0 and 5

function craftHighMPCandidates(numberOfCandidates: number) {
  const goodDigits = [1, 2, 3, 4, 6, 7, 8, 9];

  const candidates: Array<number> = [];

  for (let times = 0; times < numberOfCandidates; times++) {
    let candidateStringRepresentation = "";

    while (Number(candidateStringRepresentation) < Number.MAX_SAFE_INTEGER) {
      const randomIndex = Math.floor(Math.random() * goodDigits.length);

      const nextNumber = candidateStringRepresentation + goodDigits[randomIndex];

      if (Number(nextNumber) > Number.MAX_SAFE_INTEGER) break;
      candidateStringRepresentation = nextNumber;
    }

    candidates.push(Number(candidateStringRepresentation));
  }

  return Object.fromEntries(
    candidates.map<[number, number]>((c) => [c, computeMultiperse(c)])
  );
}

console.log("Creating 10 random candidates and computing their multiperse");
console.table(craftHighMPCandidates(10));

// 3.
// a. 1 is bad, bc 1 * x = x
// so 1s basically disappear next round of multiplication
// without adding anything to the next number

function craftHighMPCandidatesWithoutOnes(numberOfCandidates: number) {
  const goodDigits = [2, 3, 4, 6, 7, 8, 9];

  const candidates: Array<number> = [];

  for (let times = 0; times < numberOfCandidates; times++) {
    let candidateStringRepresentation = "";

    while (Number(candidateStringRepresentation) < Number.MAX_SAFE_INTEGER) {
      const randomIndex = Math.floor(Math.random() * goodDigits.length);

      const nextNumber = candidateStringRepresentation + goodDigits[randomIndex];

      if (Number(nextNumber) > Number.MAX_SAFE_INTEGER) break;
      candidateStringRepresentation = nextNumber;
    }

    candidates.push(Number(candidateStringRepresentation));
  }

  return Object.fromEntries(
    candidates.map<[number, number]>((c) => [c, computeMultiperse(c)])
  );
}

console.log("Creating 10 random candidates without 1s and computing their multiperse");
console.table(craftHighMPCandidatesWithoutOnes(10));

// b. order is important
// bc of distributivity, 53142 and 12345 will have the same multiperse

function craftHighMPCandidatesOrderedWithoutOnes(numberOfCandidates: number) {
  const goodDigits = [2, 3, 4, 6, 7, 8, 9];

  const candidates: Array<number> = [];

  for (let times = 0; times < numberOfCandidates; times++) {
    let candidateStringRepresentation = "";

    while (Number(candidateStringRepresentation) < Number.MAX_SAFE_INTEGER) {
      const randomIndex = Math.floor(Math.random() * goodDigits.length);

      const nextNumber = candidateStringRepresentation + goodDigits[randomIndex];
      const nextOrderedNumber = nextNumber.split("").sort().join("");

      if (Number(nextOrderedNumber) > Number.MAX_SAFE_INTEGER) break;
      candidateStringRepresentation = nextOrderedNumber;
    }

    candidates.push(Number(candidateStringRepresentation));
  }

  return Object.fromEntries(
    candidates.map<[number, number]>((c) => [c, computeMultiperse(c)])
  );
}

console.log("Creating 10 smallest random candidates without 1s and computing their multiperse");
console.table(craftHighMPCandidatesOrderedWithoutOnes(10));

// │ 2223346677788899 │ 7 │  ヾ(⌐■_■)ノ♪