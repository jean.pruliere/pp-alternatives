# Multiperse

First of all, don't waste your time googling `Multiperse`, it is a term the author of this exercise invented. It resembles "multiverse" but it is actually a short version of "multiplicative persistence". Today, we are gonna talk about maths, yay!

## What is multiplicative persistence?

It is very simple, actually:

- Take a integer (e.g: 487)
- Multiply every digit together to get the next number (e.g: 4 x 8 x 7 = 224)
- Repeat this step until you get a one digit number (e.g: 2 x 2 x 4 = 16, 1 x 6 = 6)

The multiplicative persistence of a number is the number of steps required to come down to a one digit number. For 487, it took 3 steps to end at 6.

### Wait, is it a real thing?

Yes, very real. It is actually part of a [research field](https://scholar.google.com/scholar?hl=en&q=multiplicative+persistence): people are paid to multiply digits together.

### But why?

We don't know. But why don't we help them since it is so simple! And fun, right?

## Let's get to it

Pick your favorite language and open Vim (jk, use your fave IDE as well).

1. Let's start small by creating the function that computes the multiplicative persistence of an integer. Run it with 487 to check if it returns 3.
2. Now let's see if we can find a number with a multiperse of **7**. FYI, the biggest multiperse ever found is 11. Stop at the lowest possible integer.
3. Try to run your function with values from 0 to a million and record how many numbers have a multiperse of 1, 2 etc.

## Paradigm shift

Ok, for now, we were focusing on a **checking** approach, running numbers through the function one by one. We need to start **searching** from good candidates.

1. The goal being to find the greatest multiperse possible, think about simple rules to eliminate numbers that will obviously score very low persistence (1 or 2). Try to run your script from 1 to 100 millions after applying those rules.
2. Write a script that crafts 100 random numbers by putting digits together and compute their persistence. Display the results so we can easily identify rules to get a better persistence while seeking the lowest possible number.
3. Apply those rules progressively and see if you can find a number with a multiperse of 10.

## Time to conclude

Nice! By now, you should have a pretty clear vision as to why it is not that simple to find a number with a "big" persistence. We still don't know why people are searching for it, though. To finish, why don't you take some time, if there is any left, to talk with your peers about what you would do if a math research program theoretically provided you with infinite computing power to find the smallest number with a persistence of 12?
